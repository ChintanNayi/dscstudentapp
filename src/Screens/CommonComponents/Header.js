import React, {useState, Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Button, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import logo from '../../Assets/dsc-logo.png'
import { Avatar } from 'react-native-elements';
import FIcon from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native';

const Header = (props) =>  {
    const navigation = useNavigation();
    const [isShowing, setIsShowing] = useState(false);

        return (
            <View style={styles.container}>
            <View style={styles.head}>
                <Text style={styles.title}>
                    Blue Mountain Driving School
                </Text>
                <Image style={styles.image} source={logo} />
            </View>
            <View style={styles.userView}>
                <View style={styles.imgView}>
                    <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
                        <Icon name="home" style={{ paddingTop: 3, paddingLeft: 5 }} color='grey' size={25} />
                    </TouchableOpacity>
                </View>

                <Text style={styles.user}>
                    {props.title}
                </Text>
                <View style={styles.imgView}>
                        <Avatar
                            containerStyle={{ alignSelf: 'center', backgroundColor: 'grey', marginTop:5}}
                            rounded
                            icon={{ name: 'user-alt', type: 'font-awesome-5' }}
                            onAccessoryPress={() => Alert.alert("change avatar")}
                            overlayContainerStyle={{ backgroundColor: 'grey' }}
                            size={25}
                            onPress={() =>  setIsShowing(!isShowing)}
                        />
                        {isShowing === true ?
                            <View style={styles.profileView}>
                                <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Avatar
                                            containerStyle={{ alignSelf: 'center', backgroundColor: 'grey', }}
                                            rounded
                                            icon={{ name: 'user-alt', type: 'font-awesome-5' }}
                                            onAccessoryPress={() => Alert.alert("change avatar")}
                                            overlayContainerStyle={{ backgroundColor: 'grey' }}
                                            size={15}
                                            onPress={() => console.log("Works!")}
                                        />
                                        <Text style={{ color: 'grey', paddingLeft: 5 }}>Profile</Text>
                                    </View>
                                </TouchableOpacity>
                                <View style={{ borderWidth: 1, borderColor: 'grey' }} />
                                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <FIcon name="log-out" style={{ paddingTop: 3, }} color='grey' size={15} />
                                        <Text style={{ color: 'grey', paddingLeft: 5 }}>Log Out</Text>
                                    </View>
                                </TouchableOpacity>
                            </View> : null}
                    </View>
            </View>
        </View>
        )
    
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        alignItems: 'center',
       
    },
    head: {
        flexDirection: 'row',
        marginTop: 50,
        width: wp('80%'),
        justifyContent: 'space-between',
        marginLeft: 30,
    },
    title: {
        color: 'grey',
        fontWeight: 'bold',
        fontSize: 20
    },
    image: {
        width: wp('7%'),
        height: hp('3%'),
        alignSelf: 'flex-end',
        marginLeft: 20
    },
    userView: {
        flexDirection: 'row',
        marginTop: 40,
        width: wp('90%'),
        justifyContent: 'space-between',
        paddingBottom: 10
    },
    user: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 20,
        paddingTop: 5,
        color: '#00a64f'
    },
    userImg: {
        width: wp('7%'),
        height: hp('3%'),
        alignSelf: 'center',
        marginTop: 3
    },
    imgView: {
        backgroundColor: '#FFFFFF',
        width: wp('10%'),
        height: hp('4%'),
        borderRadius: 4
    },
    profileView: {
        width: wp('20%'),
        backgroundColor: 'white',
        alignContent: 'center',
        borderRadius: 5,
        top:30,
        right:0,
        padding: 5,
        marginTop:5,
        position: 'absolute'
    }
})

export default Header