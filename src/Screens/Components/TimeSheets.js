import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Button, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import logo from '../../Assets/dsc-logo.png'
import { Avatar } from 'react-native-elements';
import FIcon from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome5';
import CalendarStrip from 'react-native-calendar-strip';
import Header from '../CommonComponents/Header';

export default class TimeSheets extends Component {
    constructor(props) {
        super(props)

        this.state = {
            show: false
        }
    }

    render() {
        return (
            <View style={styles.container}>
              
            <Header title={'Timesheet'} />
                <View>
                    <CalendarStrip
                        scrollable
                        calendarAnimation={{ type: 'sequence', duration: 30 }}
                        daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: '#00a64f' }}
                        style={{ height: hp('10%'), width: wp('90%'), paddingTop: 20, paddingBottom: 10, backgroundColor: 'transparent' }}
                        calendarHeaderStyle={{ color: 'black' }}
                        calendarColor={'#7743CE'}
                        dateNumberStyle={{ color: 'black' }}
                        dateNameStyle={{ color: 'black' }}
                        highlightDateNumberStyle={{ color: '#00a64f' }}
                        highlightDateNameStyle={{ color: '#00a64f' }}
                        disabledDateNameStyle={{ color: '#00a64f' }}
                        disabledDateNumberStyle={{ color: '#00a64f' }}
                        iconContainer={{ flex: 0.1 }}
                    />
                    <ScrollView>

                        <View style={styles.mainView}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                                <View style={{ flexDirection: 'row' }}>
                                    <FIcon name="file-text" style={{ paddingTop: 3, paddingLeft: 5 }} color='#feba24' size={45} />
                                    <View style={{ flexDirection: 'column', paddingTop: 5 }}>
                                        <View style={{ flexDirection: 'row', paddingLeft: 5 }}>
                                            <Text style={{ color: '#00a64f' }}>Course 1</Text>
                                            <Text style={{ paddingLeft: 5 }}>(Theory)</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', paddingLeft: 5, paddingTop: 5 }}>
                                            <Text style={{ color: 'grey' }}>Start Time :10am</Text>
                                            <Text style={{ paddingLeft: 5, color: 'grey' }}>End Time :12pm</Text>
                                        </View>
                                    </View>
                                </View>

                                {
                                    this.state.show === true ?
                                        <TouchableOpacity onPress={() => this.setState({ show: !this.state.show })}>
                                            <Icon name="chevron-circle-up" color='grey' style={{ paddingRight: 15, paddingTop: 20 }} size={30} />
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={() => this.setState({ show: !this.state.show })}>
                                            <Icon name="chevron-circle-down" color='grey' style={{ paddingRight: 15, paddingTop: 20 }} size={30} />
                                        </TouchableOpacity>
                                }
                            </View>
                            {
                                this.state.show === true ?
                                    <View>
                                        <View style={{ width: wp('100%'), borderColor: 'grey', borderWidth: 1, marginTop: 10 }} />
                                        <View style={{ flexDirection: 'column', paddingTop: 5, alignItems: 'center' }}>
                                            <Text style={{ color: 'grey' }}>Instructore Add Start Time / End Time</Text>
                                            <View style={{ flexDirection: 'row', paddingLeft: 5, paddingTop: 5 }}>
                                                <Text style={{ color: 'grey', marginRight: 27 }}>Start Time : 10am</Text>
                                                <Text style={{ paddingLeft: 5, color: 'grey' }}>End Time : 12pm</Text>
                                            </View>
                                        </View>
                                    </View> : null}
                        </View>
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        alignItems: 'center',
    },
    head: {
        flexDirection: 'row',
        marginTop: 50,
        width: wp('80%'),
        justifyContent: 'space-between',
        marginLeft: 30,
    },
    title: {
        color: 'grey',
        fontWeight: 'bold',
        fontSize: 20
    },
    image: {
        width: wp('7%'),
        height: hp('3%'),
        alignSelf: 'flex-end',
        marginLeft: 20
    },
    userView: {
        flexDirection: 'row',
        marginTop: 40,
        width: wp('90%'),
        justifyContent: 'space-between',
        paddingBottom: 10
    },
    user: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 20,
        paddingTop: 5,
        color: '#00a64f'
    },
    userImg: {
        width: wp('7%'),
        height: hp('3%'),
        alignSelf: 'center',
        marginTop: 3
    },
    imgView: {
        backgroundColor: '#FFFFFF',
        width: wp('10%'),
        height: hp('4%'),
        borderRadius: 4
    },
    mainView: {
        width: wp('95%'),
        backgroundColor: '#FFFFFF',
        marginTop: 20,
        borderRadius: 5,
        paddingBottom: 10
    },
})