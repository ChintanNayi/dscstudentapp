import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Button, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Header from '../CommonComponents/Header'
import { Avatar, CheckBox } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class PaymentsList extends Component {

    constructor(props) {
        super(props)
        super(props)

        this.state = {
            checked: false,
            options: [
                {
                    key: 'pay',
                    text: 'Email Id',
                },
                {
                    key: 'performance',
                    text: 'Phone Number',
                },
            ],
            selectedOption: 'pay',
            show: false
        }
        this.onSelect = this.onSelect.bind(this);
    }

    onSelect = (item) => {
        this.setState({ selectedOption: item })
        console.log("test", this.state.selectedOption, item)
        if (this.state.selectedOption === item.key) {
            this.setState({ selectedOption: null })
        } else {
            this.setState({ selectedOption: item.key })
        }
    };

    render() {
        return (
            <View style={styles.container}>
            <Header title={'Payments'} />
                <View style={styles.header}>
                    <View style={{ flexDirection: 'column', alignItems: 'center', }}>
                        <Text style={styles.headTitle}>Total Course Fee</Text>
                        <Text>$1000</Text>
                    </View>
                    <View style={{ flexDirection: 'column' }}>
                        <Text style={styles.headTitle}>Fee Paid</Text>
                        <Text style={{ paddingLeft: 10, color: '#00a64f' }}>$300</Text>
                    </View>
                    <View style={{ flexDirection: 'column' }}>
                        <Text style={styles.headTitle}>Remaining Fee</Text>
                        <Text style={{ paddingLeft: 30, color: '#fba00c' }}>$700</Text>
                    </View>
                </View>
                <ScrollView>

                    <View style={styles.mainView}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start', alignContent: 'flex-start' }}>
                            <View>
                                <Text style={{ color: 'black', paddingTop: 10, paddingLeft: 10 }}>Program Name</Text>
                                <Text style={{ color: 'black', paddingTop: 5, paddingLeft: 10 }}>Lorem Ipsum is dummy Text</Text>
                            </View>
                            {
                                this.state.show === true ?
                                    <TouchableOpacity onPress={() => this.setState({ show: !this.state.show })}>
                                        <Icon name="chevron-circle-up" color='grey' style={{ paddingRight: 15, paddingTop: 15 }} size={30} />
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity onPress={() => this.setState({ show: !this.state.show })}>
                                        <Icon name="chevron-circle-down" color='grey' style={{ paddingRight: 15, paddingTop: 15 }} size={30} />
                                    </TouchableOpacity>
                            }

                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start', alignContent: 'flex-start' }}>
                            <View>
                                <Text style={{ color: 'black', paddingTop: 10, paddingLeft: 10 }}>Total Fee</Text>
                                <Text style={{ color: 'black', paddingTop: 5, paddingLeft: 10 }}>$1000.00</Text>
                            </View>
                            <View>
                                <Text style={{ color: 'black', paddingTop: 10, }}>Total Part Payment</Text>
                                <Text style={{ color: 'black', paddingTop: 5 }}>4</Text>
                            </View>
                            <View>
                                <Text style={{ color: 'black', paddingTop: 10, paddingRight: 15 }}>Total Free Paid</Text>
                                <Text style={{ color: 'black', paddingTop: 5 }}>$500.00</Text>
                            </View>
                        </View>

                        {this.state.show === true ?
                            <View>
                                <View style={{ width: wp('100%'), borderColor: 'grey', borderWidth: 1, marginTop: 10 }} />
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start', alignContent: 'flex-start' }}>
                                    <View style={{ marginLeft: 63 }} />
                                    <Text style={{ paddingTop: 15 }}>Part Payment 1</Text>
                                    <Text style={{ paddingTop: 15 }}>250.00$</Text>
                                    <View style={styles.compliteView}>
                                        <Text style={{ color: 'white', paddingTop: 3 }}>Completed</Text></View>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start', alignContent: 'flex-start' }}>
                                    <CheckBox
                                        checkedColor='#00a64f'
                                        checked={this.state.checked}
                                        onPress={() => this.setState({ checked: !this.state.checked })}
                                    />
                                    <Text style={{ paddingTop: 15 }}>Part Payment 1</Text>
                                    <Text style={{ paddingTop: 15 }}>250.00$</Text>
                                    <View style={styles.paddingView}>
                                        <Text style={{ color: 'white', paddingTop: 3 }}>Pending</Text></View>
                                </View>
                            </View>
                            : null}

                    </View>
                    {this.state.show === true ?
                        <View style={styles.mainView}>
                            <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View>
                                    <Text>Course Fee</Text>
                                    <TextInput
                                        style={styles.input}
                                        onChangeText={(text) => this.setState({ courseFee: text })}
                                        placeholder='$00.00'
                                        underlineColorAndroid="transparent"
                                        value={this.state.courseFee}
                                        keyboardType="number-pad"
                                    />
                                </View>
                                <View>
                                    <Text>Installment Premium</Text>
                                    <TextInput
                                        style={styles.input}
                                        onChangeText={(text) => this.setState({ installment: text })}
                                        placeholder='$00.00'
                                        underlineColorAndroid="transparent"
                                        value={this.state.installment}
                                        keyboardType="number-pad"
                                    />
                                </View>

                            </View>
                            <View style={{ padding: 10, }}>
                                <Text>Total Amount</Text>
                                <TextInput
                                    style={styles.fullinput}
                                    onChangeText={(text) => this.setState({ total: text })}
                                    placeholder='$00.00'
                                    underlineColorAndroid="transparent"
                                    value={this.state.total}
                                    keyboardType="number-pad"
                                    
                                />
                            </View>
                            <TouchableOpacity  style={styles.loginBtn}>
                                <Text style={styles.loginText}>Pay Now</Text>
                            </TouchableOpacity>

                        </View> : null}

                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    header: {
        marginTop: 45,
        width: wp('95%'),
        height: hp('7%'),
        backgroundColor: '#FFFFFF',
        borderRadius: 4,
        justifyContent: 'space-between',
        flexDirection: 'row',
        padding: 10
    },
    mainView: {
        width: wp('95%'),
        backgroundColor: '#FFFFFF',
        marginTop: 20,
        borderRadius: 5,
        paddingBottom: 10
    },
    headTitle: {
        color: 'black',
        fontWeight: 'bold'
    },
    compliteView: {
        backgroundColor: '#00a64f',
        alignItems: 'center',
        marginRight: 20,
        marginTop: 13,
        width: wp('25%'),
        height: hp('3%'),
        borderRadius: 15
    },
    paddingView: {
        backgroundColor: '#fba00c',
        alignItems: 'center',
        marginRight: 20,
        marginTop: 13,
        width: wp('25%'),
        height: hp('3%'),
        borderRadius: 15
    },
    input: {
        height: hp('5%'),
        width: wp('40%'),
        borderColor: 'gray',
        borderBottomWidth: 1,
    },
    fullinput: {
        height: hp('5%'),
        width: wp('90%'),
        borderColor: 'gray',
        borderBottomWidth: 1,
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('90%'),
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#00a64f",
        marginLeft: 10
    },
})
