import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Button, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Header from '../CommonComponents/Header'

export default class FeedBack extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Header title={'Feed Back'} />
                <ScrollView style={{marginTop:35}}>
                    <View style={styles.mainView}>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Class Name</Text>
                            <Text style={{paddingRight:125,paddingTop:10,color:'black'}}>Class Name 1</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Timestamp</Text>
                            <Text style={{paddingRight:65,paddingTop:10,color:'black'}}>16, July 2021 | 11:30am</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Instructore Name</Text>
                            <Text style={{paddingRight:105,paddingTop:10,color:'black'}}>instructore name</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Message</Text>
                            <Text style={{paddingRight:20,paddingTop:10,color:'black'}}>Lorem Ipsum is simply dummy</Text>
                        </View>
                    </View>
                    <View style={styles.mainView}>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Class Name</Text>
                            <Text style={{paddingRight:125,paddingTop:10,color:'black'}}>Class Name 1</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Timestamp</Text>
                            <Text style={{paddingRight:65,paddingTop:10,color:'black'}}>16, July 2021 | 11:30am</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Instructore Name</Text>
                            <Text style={{paddingRight:105,paddingTop:10,color:'black'}}>instructore name</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Message</Text>
                            <Text style={{paddingRight:20,paddingTop:10,color:'black'}}>Lorem Ipsum is simply dummy</Text>
                        </View>
                    </View>
                    <View style={styles.mainView}>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Class Name</Text>
                            <Text style={{paddingRight:125,paddingTop:10,color:'black'}}>Class Name 1</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Timestamp</Text>
                            <Text style={{paddingRight:65,paddingTop:10,color:'black'}}>16, July 2021 | 11:30am</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Instructore Name</Text>
                            <Text style={{paddingRight:105,paddingTop:10,color:'black'}}>instructore name</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Message</Text>
                            <Text style={{paddingRight:20,paddingTop:10,color:'black'}}>Lorem Ipsum is simply dummy</Text>
                        </View>
                    </View>
                    <View style={styles.mainView}>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Class Name</Text>
                            <Text style={{paddingRight:125,paddingTop:10,color:'black'}}>Class Name 1</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Timestamp</Text>
                            <Text style={{paddingRight:65,paddingTop:10,color:'black'}}>16, July 2021 | 11:30am</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Instructore Name</Text>
                            <Text style={{paddingRight:105,paddingTop:10,color:'black'}}>instructore name</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-start',alignContent:'flex-start'}}>
                            <Text style={{color:'black',fontWeight:'bold',paddingTop:10,paddingLeft:20}}>Message</Text>
                            <Text style={{paddingRight:20,paddingTop:10,color:'black'}}>Lorem Ipsum is simply dummy</Text>
                        </View>
                    </View>

                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    mainView: {
        width: wp('95%'),
        height: hp('15%'),
        backgroundColor: '#FFFFFF',
        marginTop: 10,
        borderRadius: 5,
        paddingBottom:10
    },
})
