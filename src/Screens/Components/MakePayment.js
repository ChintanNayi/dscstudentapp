import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Button, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Header from '../CommonComponents/Header';
import Icon from 'react-native-vector-icons/FontAwesome5';
import EIcon from 'react-native-vector-icons/EvilIcons';

export default class MakePayment extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    
    render() {
        return (
            <View style={styles.container}>
                <Header title={'Make Payments'} />
                <View style={styles.mainView}>
                    <View style={{ flexDirection: 'row' }}>
                        <EIcon name="lock" color='grey' size={25} />
                        <Text style={{ color: 'black' }}>SECURE SSL ENCRIPTED PAYMENT</Text>
                    </View>
                    <View style={{ flexDirection: 'row', padding: 5 }}>
                        <Icon name="cc-mastercard" color='green' size={35} />
                        <Icon name="credit-card" color='green' style={{ marginLeft: 10 }} size={35} />
                    </View>
                    <Text style={{ color: 'black',padding:5, fontSize: 15 }}>Payment Information(require)</Text>
                    <Text style={{ color: 'black',padding:5 }}>CREDIT CARD/DEBIT CARD</Text>
                    <View style={{ padding: 5, }}>
                        <Text>Card holder name</Text>
                        <TextInput
                            style={styles.fullinput}
                            onChangeText={(text) => this.setState({ cname: text })}
                            placeholder='Enter card holder name'
                            underlineColorAndroid="transparent"
                            value={this.state.cname}
                        />
                    </View>
                    <View style={{ padding: 5, }}>
                        <Text>Card number</Text>
                        <TextInput
                            style={styles.fullinput}
                            onChangeText={(text) => this.setState({ cnumber: text })}
                            placeholder='Enter Card number'
                            underlineColorAndroid="transparent"
                            value={this.state.cnumber}
                            keyboardType="number-pad"
                        />
                    </View>
                    <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'space-between' }}>

                        <View>
                            <Text>Add month/year</Text>
                            <TextInput
                                style={styles.input}
                                onChangeText={(text) => this.setState({ monthYear: text })}
                                placeholder='MM/YY'
                                underlineColorAndroid="transparent"
                                value={this.state.monthYear}
                                keyboardType="number-pad"
                            />
                        </View>
                        <View>
                            <Text>CVV number</Text>
                            <TextInput
                                style={styles.input}
                                onChangeText={(text) => this.setState({ cvv: text })}
                                placeholder='Add CVV number'
                                underlineColorAndroid="transparent"
                                value={this.state.cvv}
                                secureTextEntry
                                maxLength={3}
                                keyboardType="number-pad"
                            />
                        </View>

                    </View>
                    <View style={{ padding: 10, }}>
                        <Text>Total Amount</Text>
                        <TextInput
                            style={styles.fullinput}
                            onChangeText={(text) => this.setState({ total: text })}
                            placeholder='$260'
                            underlineColorAndroid="transparent"
                            value={this.state.total}
                            keyboardType="number-pad"
                        />
                    </View>
                    <TouchableOpacity style={styles.loginBtn}>
                        <Text style={styles.loginText}>Pay</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    mainView: {
        width: wp('95%'),
        backgroundColor: '#FFFFFF',
        marginTop: 45,
        borderRadius: 5,
        padding: 10
    },
    input: {
        height: hp('5%'),
        width: wp('40%'),
        borderColor: 'gray',
        borderBottomWidth: 1,
    },
    fullinput: {
        height: hp('5%'),
        width: wp('85%'),
        borderColor: 'gray',
        borderBottomWidth: 1,
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('90%'),
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#00a64f",
        marginLeft: 10
    },
})
