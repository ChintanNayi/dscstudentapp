import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Button, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import logo from '../../Assets/dsc-logo.png'
import { Avatar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class Dashboard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            showProfile: false
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.head}>
                    <Text style={styles.title}>
                        Blue Mountain Driving School
                    </Text>
                    <Image style={styles.image} source={logo} />
                </View>
                <View style={styles.userView}>
                    <Text style={styles.user}>
                        Smith D.Jeo
                    </Text>
                    <View style={styles.imgView}>
                        <Avatar
                            containerStyle={{ alignSelf: 'center', backgroundColor: 'grey', marginTop: 5 }}
                            rounded
                            icon={{ name: 'user-alt', type: 'font-awesome-5' }}
                            onAccessoryPress={() => Alert.alert("change avatar")}
                            overlayContainerStyle={{ backgroundColor: 'grey' }}
                            size={25}
                            onPress={() => this.setState({ showProfile: !this.state.showProfile })}
                        />
                        {this.state.showProfile === true ?
                            <View style={styles.profileView}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Avatar
                                            containerStyle={{ alignSelf: 'center', backgroundColor: 'grey', }}
                                            rounded
                                            icon={{ name: 'user-alt', type: 'font-awesome-5' }}
                                            onAccessoryPress={() => Alert.alert("change avatar")}
                                            overlayContainerStyle={{ backgroundColor: 'grey' }}
                                            size={15}
                                            onPress={() => console.log("Works!")}
                                        />
                                        <Text style={{ color: 'grey', paddingLeft: 5 }}>Profile</Text>
                                    </View>
                                </TouchableOpacity>
                                <View style={{ borderWidth: 1, borderColor: 'grey' }} />
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Icon name="log-out" style={{ paddingTop: 3, }} color='grey' size={15} />
                                        <Text style={{ color: 'grey', paddingLeft: 5 }}>Log Out</Text>
                                    </View>
                                </TouchableOpacity>
                            </View> : null}
                    </View>
                </View>
                <View style={styles.header}>
                    <Text style={styles.headText}>Dashboard</Text>
                </View>
                <View style={styles.mainView}>
                    <ScrollView>
                        <View style={{ flexDirection: 'column', marginBottom: 20 }}>
                            <View style={{ flexDirection: 'row' }}>
                               
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Timesheet')}>
                                        <View style={styles.itemView}>
                                            <FontAwesome name="file-o" style={{ marginTop: 35 }} color='#feba24' size={55} />
                                            <Text style={{ marginTop: 20, color: 'black', fontSize: 15 }}>Time Sheet</Text>
                                        </View>
                                    </TouchableOpacity>
                                
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('FeedBack')}>
                                    <View style={styles.itemView}>
                                        <FontAwesome name="file-text-o" style={{ marginTop: 35 }} color='#feba24' size={55} />
                                        <Text style={{ marginTop: 20, color: 'black', fontSize: 15 }}>Feed Back</Text>
                                    </View>
                                </TouchableOpacity>

                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('PaymentList')}>
                                    <View style={styles.itemView}>
                                        <FontAwesome5 name="file-invoice-dollar" style={{ marginTop: 35 }} color='#feba24' size={55} />
                                        <Text style={{ marginTop: 20, color: 'black', fontSize: 15 }}>Payments List</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('History')}>
                                    <View style={styles.itemView}>
                                        <FontAwesome name="history" style={{ marginTop: 35 }} color='#feba24' size={55} />
                                        <Text style={{ marginTop: 20, color: 'black', fontSize: 15 }}>History</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('MakePayment')}>
                                    <View style={styles.itemView}>
                                        <MaterialIcons name="payment" style={{ marginTop: 35 }} color='#feba24' size={55} />
                                        <Text style={{ marginTop: 20, color: 'black', fontSize: 15 }}>Make Payments</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                                    <View style={styles.itemView}>
                                        <FontAwesome name="user-circle" style={{ marginTop: 35 }} color='#feba24' size={55} />
                                        <Text style={{ marginTop: 20, color: 'black', fontSize: 15 }}>Profile</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>

                        </View>

                    </ScrollView>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    head: {
        flexDirection: 'row',
        marginTop: 50,
        width: wp('80%'),
        justifyContent: 'space-between',
        marginLeft: 30,
    },
    title: {
        color: 'grey',
        fontWeight: 'bold',
        fontSize: 20
    },
    userView: {
        flexDirection: 'row',
        marginTop: 40,
        width: wp('90%'),
        justifyContent: 'space-between'
    },
    user: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 20
    },
    image: {
        width: wp('7%'),
        height: hp('3%'),
        alignSelf: 'flex-end',
        marginLeft: 20
    },
    userImg: {
        width: wp('7%'),
        height: hp('3%'),
        alignSelf: 'center',
        marginTop: 3
    },
    imgView: {
        backgroundColor: '#FFFFFF',
        width: wp('10%'),
        height: hp('4%'),
        borderRadius: 4
    },
    header: {
        marginTop: 55,
        width: wp('90%'),
        height: hp('5%'),
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        borderRadius: 4,
    },
    headText: {
        color: '#00a64f',
        fontSize: 20,
        fontWeight: 'bold',
        paddingTop: 7,
    },
    mainView: {
        width: wp('90%'),
        height: hp('65%'),
        backgroundColor: '#FFFFFF',
        marginTop: 20,
        borderRadius: 4,
    },
    itemView: {
        borderWidth: 1,
        borderColor: 'grey',
        height: hp('20%'),
        width: wp('35%'),
        borderRadius: 10,
        margin: 20,
        alignItems: 'center'
    },
    profileView: {
        width: wp('20%'),
        backgroundColor: 'white',
        alignContent: 'center',
        borderRadius: 5,
        right: 40,
        padding: 5,
        marginTop: 5
    }
})