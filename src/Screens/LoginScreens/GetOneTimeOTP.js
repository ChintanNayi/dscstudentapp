import React, { Component } from 'react'
import { Button, View, StyleSheet, Text, TouchableOpacity, Image, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LOGO from '../../Assets/dsc-main-logo.png';


export class GetOneTimeOTP extends Component {
    constructor(props) {
        super(props)

        this.state = {
            checked: 'first',
            options: [
                {
                    key: 'pay',
                    text: 'Email Id',
                },
                {
                    key: 'performance',
                    text: 'Phone Number',
                },
            ],
            selectedOption:'pay'
        }
        this.onSelect = this.onSelect.bind(this);
    }

    onSelect = (item) => {
        this.setState({selectedOption:item})
        console.log("test",this.state.selectedOption,item)
        if (this.state.selectedOption === item.key) {
          this.setState({selectedOption:null})
        } else {
            this.setState({selectedOption:item.key})
        }
      };

    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    <View style={{ alignItems: 'center' }}>
                        <Image style={styles.image} source={LOGO} />
                    </View>
                    <Text style={styles.head}>Get One-Time Code</Text>

                    <View style={{alignSelf:'flex-start',marginLeft:60,marginTop:80}}>
                        {this.state.options.map((item) => {
                            return (
                                <View key={item.key} style={styles.buttonContainer}>
                                    
                                    <TouchableOpacity
                                        style={styles.circle}
                                        onPress={() => {
                                            this.onSelect(item);
                                        }}>
                                        {this.state.selectedOption === item.key && (
                                            <View style={styles.checkedCircle} />
                                        )}
                                    </TouchableOpacity>
                                    <Text style={{marginLeft:20, color:'black'}}>{item.text}</Text>
                                </View>
                            );
                        })}
                    </View>


                    <TouchableOpacity onPress={() => this.props.navigation.navigate('EnterOTP')} style={styles.loginBtn}>
                        <Text style={styles.loginText}>Send</Text>
                    </TouchableOpacity>

                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // textAlign:'center',
        alignItems: 'center',
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    head: {
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: 70,
        fontSize: 30,
        color: '#4d4d4d'
    },
    loginBtn:
    {
        width: wp('80%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
        marginBottom: 10,
        backgroundColor: "#00a64f",
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('50%'),
        height: hp('9%'),
        marginTop: 45
    },
    iconStyle: {
        marginTop: 30,
        marginLeft: 10 // default is 12
    },
    searchSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    radioGroup: {
        flexDirection: 'row',
        width: wp('80%'),
    },
    radioText: {
        marginTop: 7,
        textAlign: 'left'
    },
    buttonContainer: {
        flexDirection: 'row',
        marginBottom: 20,

    },
    circle: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#feba24',
        alignItems: 'center',
        justifyContent: 'center',
    },
    checkedCircle: {
        width: 10,
        height: 10,
        borderRadius: 10,
        backgroundColor: '#feba24',
    },
});

export default GetOneTimeOTP

