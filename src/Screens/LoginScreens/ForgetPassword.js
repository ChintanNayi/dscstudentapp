import React, { Component } from 'react'
import { Button, View, StyleSheet, Text, TextInput, TouchableOpacity, Image, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import axios from 'axios';
import { SafeAreaView } from 'react-native-safe-area-context';
import LOGO from '../../Assets/dsc-main-logo.png';
import { Input } from 'react-native-elements';

export class ForgetPassword extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: ''
        }
    }

    SignIn = () => {
        const nav = this.props.navigation
        const ldata = { email: this.state.email, password: this.state.password }
        const headers = {
            'Content-Type': 'multipart/form-data'
        }

        nav.navigate('SendOTP');

        // axios
        //     .post('https://vibrantsofttech.com/project/prosourceadvisory/api/authentication/login', ldata, {
        //         headers: headers
        //     })
        //     .then(function (response) {
        //         // handle success
        //         console.log("res", response.data.status)

        //         if (response.data.status === true) {
        //             nav.navigate('SendOTP');
        //             console.log("ifff")
        //         }
        //     })
        //     .catch(function (error) {
        //         // handle error
        //         alert(error.message);
        //     });
    }

    render() {
        return (
            <ScrollView>
                <SafeAreaView style={styles.container}>

                    <View style={{ alignItems: 'center' }}>
                        <Image style={styles.image} source={LOGO} /></View>
                    <Text style={styles.head}>Forget Password</Text>
                    <View style={{width:wp('90%')}}>
                    <Input
                        style={styles.input}
                        onChangeText={(text) => this.setState({ email: text })}
                        label='Email-Id'
                        placeholder='test@gmail.com'
                    />
                    </View>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('SendOTP')} style={styles.loginBtn}>
                        <Text style={styles.loginText}>Call to Action</Text>
                    </TouchableOpacity>

                </SafeAreaView>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop:100
    },
    head: {
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: 50,
        paddingBottom: 30,
        fontSize: hp('3%'),
        color: '#4d4d4d'
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('80%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
        marginBottom: 10,
        backgroundColor: "#00a64f",
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('50%'),
        height: hp('9%'),
        marginBottom: 10,
        marginTop: 5
    },
});

export default ForgetPassword
