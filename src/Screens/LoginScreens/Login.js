import React, { Component } from 'react'
import { Button, View, StyleSheet, Text, TextInput, TouchableOpacity, Image, ScrollView,SafeAreaView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LOGO from '../../Assets/dsc-main-logo.png';
import * as Animatable from 'react-native-animatable';


export class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: '',
            animation:''
        }
    }


    render() {
       const MyCustomComponent = Animatable.createAnimatableComponent(TouchableOpacity);
        return (
            <ScrollView>
                <SafeAreaView style={styles.container}>
                    <View style={{ alignItems: 'center' }}>
                        <Image style={styles.image} source={LOGO} />
                    </View>

                    <Text style={styles.head}>Sig In</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(text) => this.setState({ email: text })}
                        placeholder='User Name'
                        underlineColorAndroid="transparent"
                        value={this.state.email}
                    />

                    <TextInput
                        style={styles.input}
                        onChangeText={(text) => this.setState({ password: text })}
                        placeholder='Password'
                        underlineColorAndroid="transparent"
                        value={this.state.password}
                        secureTextEntry
                    />
                    <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgetPassword')} style={styles.bottom}>
                        <Text style={styles.forgot_button}>Forget Password?</Text>
                    </TouchableOpacity>
                      </View>
                    <MyCustomComponent animation={this.state.animation} onPress={() => this.setState({animation:'wobble'})} style={styles.loginBtn}>
                    <Text style={styles.loginText}>Sign In</Text>
                    </MyCustomComponent>

                </SafeAreaView>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    head: {
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: 30,
        paddingBottom:30,
        fontSize: hp('4%'),
        color: '#4d4d4d'
    },
    title: {
        height: hp('4%'),
        marginBottom: 20,
        textAlign: 'center',
        color: 'grey',
        marginTop: 20
    },
    input: {
        height: hp('5%'),
        width: wp('80%'),
        borderColor: 'gray',
        borderBottomWidth: 1,
        margin: 10
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('80%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
        marginBottom: 10,
        backgroundColor: "#00a64f",
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('50%'),
        height: hp('9%'),
        marginTop: 45
    },
    forgot_button: {
        textAlign: 'right',
        flex: 1,
        marginRight: 50,
        marginTop: 20
    },
    iconStyle: {
        marginTop: 30,
        marginLeft: 10 // default is 12
    },
    searchSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
      }
});

export default Login
