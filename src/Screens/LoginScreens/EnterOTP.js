import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Button, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LOGO from '../../Assets/dsc-main-logo.png';

export class EnterOTP extends Component {
    constructor(props) {
        super(props)

        this.state = {
            timer: null,
            counter: 59,
            phone: '+919924460329',
            confirmResult: null,
            verificationCode: '',
            userId: ''
        }

    }

    componentDidMount() {
        this.startTimer();
    }

    handleVerifyCode = () => {
        // Request for OTP verification
        const nav = this.props.navigation
        nav.navigate('ScreenExternal');
    }

    startTimer = () => {
        this.setState({
            show: false
        })
        let timer = setInterval(this.manageTimer, 1000);
        this.setState({ timer });

    }

    manageTimer = () => {

        var states = this.state

        if (states.counter === 0) {
            clearInterval(this.state.timer)
            this.setState({
                counter: 59,
                show: true
            })
        }
        else {
            this.setState({
                counter: this.state.counter - 1
            });

        }
    }

    componentWillUnmount() {
        clearInterval(this.state.timer);
    }


    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                <View style={{ alignItems: 'center' }}>
                        <Image style={styles.image} source={LOGO} />
                    </View>
                    <Text style={styles.head}>Get Your One-Time Code</Text>
                    <View style={styles.inputGroup}>
                        <TextInput
                            ref={'input_1'}
                            keyboardType="number-pad"
                            style={styles.input}
                            onChangeText={value => {
                                this.setState({ value })
                                if (value) this.refs.input_2.focus(); //assumption is TextInput ref is input_2
                            }}
                            placeholder='0'
                            maxLength={1}
                            underlineColorAndroid="transparent"
                        // value={this.state.text}
                        />

                        <TextInput
                            ref={'input_2'}
                            keyboardType="number-pad"
                            style={styles.input}
                            onChangeText={value => {
                                this.setState({ value })
                                if (value) this.refs.input_3.focus(); //assumption is TextInput ref is input_2
                            }}
                            placeholder='0'
                            maxLength={1}
                            underlineColorAndroid="transparent"
                        // value={input}
                        />
                        <TextInput
                            ref={'input_3'}
                            keyboardType="number-pad"
                            style={styles.input}
                            maxLength={1}
                            onChangeText={value => {
                                this.setState({ value })
                                if (value) this.refs.input_4.focus(); //assumption is TextInput ref is input_2
                            }}
                            placeholder='0'
                            underlineColorAndroid="transparent"
                        // value={input}
                        />
                        <TextInput
                            ref={'input_4'}
                            style={styles.input}
                            keyboardType="number-pad"
                            maxLength={1}
                            onChangeText={value => {
                                this.setState({ value })
                                if (value) this.refs.input_5.focus(); //assumption is TextInput ref is input_2
                            }}
                            placeholder='0'
                            underlineColorAndroid="transparent"
                        // value={input}
                        />
                        <TextInput
                            ref={'input_5'}
                            keyboardType="number-pad"
                            style={styles.input}
                            onChangeText={value => {
                                this.setState({ value })
                                if (value) this.refs.input_6.focus(); //assumption is TextInput ref is input_2
                            }}
                            placeholder='0'
                            maxLength={1}
                            underlineColorAndroid="transparent"
                        // value={input}
                        />
                        <TextInput
                            ref={'input_6'}
                            keyboardType="number-pad"
                            style={styles.input}
                            onChangeText={value => {
                                this.setState({ value })
                            }}
                            placeholder='0'
                            maxLength={1}
                            underlineColorAndroid="transparent"
                        // value={input}
                        />
                    </View>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Dashboard')} style={styles.loginBtn}>
                        <Text style={styles.loginText}>CONTINUE</Text>
                    </TouchableOpacity>

                    {this.state.show === true ?

                        <View style={styles.timerView}>
                            <Text style={{ textAlign: 'center', marginRight: 10 }}>00:00</Text>
                            <TouchableOpacity onPress={() => this.startTimer()}>
                                <Text style={styles.resend}>Resend Code</Text>
                            </TouchableOpacity>

                        </View> :
                        <View style={styles.timerView}>
                            <Text style={{ textAlign: 'center', marginRight: 10 }}>00:{this.state.counter}</Text>
                            <Text style={styles.resend1}>Resend Code</Text>
                        </View>

                    }
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop:100
    },
    head: {
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: 70,
        fontSize: 25,
        color: '#4d4d4d'
    },
    input: {
        height: hp('5%'),
        width: wp('8%'),
        borderColor: 'gray',
        borderBottomWidth: 1,
        borderBottomColor: '#00a64f',
        textAlign: 'center',
        // marginLeft: 40,
        margin: 10
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('80%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
        backgroundColor: "#00a64f",
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('50%'),
        height: hp('9%'),
        marginTop: 45
    },
    iconStyle: {
        marginTop: 30,
        marginLeft: 10 // default is 12
    },
    inputGroup: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 40
    },
    resend: {
        textAlign: 'center',
        color: '#00a64f'
    },
    resend1: {
        textAlign: 'center',
        color: 'grey'
    },
    timerView:{
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 30,
        marginBottom:20
    }
});

export default EnterOTP
