import React, { Component } from 'react'
import { Image, View, StyleSheet, TouchableOpacity, Text } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import logo from '../../Assets/dsc-main-logo.png';
import WelcomeImg from '../../Assets/welcome.png'

export default class WelcomeScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.image} source={logo} />
                <Image style={styles.mainImg} source={WelcomeImg} />
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')} style={styles.loginBtn}>
                    <Text style={styles.loginText}>SIGN IN</Text>
                </TouchableOpacity>

                <View style={styles.bottom}>
                    <Text style={styles.textbottom}>@ 2021 Driving School Cloud. All rights reserved</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('50%'),
        height: hp('9%'),
        marginTop: 50
    },
    mainImg: {
        marginTop: 100
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
    },
    loginBtn:
    {
        width: wp('80%'),
        borderRadius: 10,
        height: hp("5%"),
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
        marginBottom: 10,
        backgroundColor: "#00a64f",
    },
    textbottom: {
        height: wp('10%'),
        color: 'grey',
        // marginTop: 50,
        top:150
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
      }
})